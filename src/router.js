import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Forums from './views/Forums.vue'
import Threads from './views/Threads.vue'
import Thread from './views/Thread.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/forums',
      name: 'forums',
      component: Forums
    },
    {
      path: '/forums/:id',
      name: 'threads',
      component: Threads
    },
    {
      path: '/threads/:id',
      name: 'thread',
      component: Thread
    }
  ]
})
